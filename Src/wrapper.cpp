#include "wrapper.hpp"

/* Include Default Begin */
#include <cmath>
/* Include Default End */


/* Include Private Begin */
#include "gpio.h"
#include "i2c.h"
#include "tim.h"
/* Include Private End */

/* Class Constructor Begin */
#define ADDRESS 0x6A<<1//モジュールのアドレス
#define GYRO_REGISTA_START 0x18//角速度が入っている先頭アドレス
#define FRENQUENCY 1000.0//tim6の周波数
//#define OFFSET
#define LOOPASS

/* Class Constructor End */


/* Variable Begin */
int16_t offset[3] = {0,0,0};
int16_t angularVelocity[3] = {0,0,0};
float deg[3] = {0,0,0};
float rad[3] = {0,0,0};
pastAndNow intergral;
angleVector3D beforeCorrect;
float buf[3] = {0,0,0};
uint8_t debug = 0;
uint8_t warikomi = 0;
uint8_t data[6] = {0,0,0,0,0,0};
uint8_t commu = 0;
float angularAccerelation[3] = {0,0,0};
float anguluarVelocityError[3] ={0,0,0};// {5575,5690,4790};
float fixAngularvelocity[3];
#ifdef LOOPASS
float loopassGain = 0.1;
#endif//LOOPASS
float checkangularVelocity[3] = {0,0,0};
uint32_t debugTick;
uint32_t tickRange = 0;
uint32_t timerCount;
uint8_t errorFlug[3] = {0,0,0};
float beforeVelocity[3] = {0,0,0};
uint8_t beforeData[6] = {0,0,0,0,0};
uint8_t countE;

/* Variable End */

void init(void){
	//CTRL_REG1_G
	HAL_TIM_Base_Start_IT(&htim6);
	uint8_t gyroConfig[]={0b11011010,0b00000011,0b00001001};//CTRL_REG1_G,CTRL_REG2_G,CTRL_REG3_G
	while(HAL_I2C_Mem_Write(&hi2c1,ADDRESS,0x10,1,gyroConfig,sizeof(gyroConfig),1000) != HAL_OK){
		HAL_GPIO_WritePin(GPIOA,GPIO_PIN_5,GPIO_PIN_SET);
		debug = 1;
	}
		HAL_GPIO_WritePin(GPIOA,GPIO_PIN_5,GPIO_PIN_RESET);
		debug = 2;


	uint8_t on = 0b00111000;
	//CTRL_REG4
	while(HAL_I2C_Mem_Write(&hi2c1,ADDRESS,0x1E,1,&on,1,100) != HAL_OK){
		debug = 3;
	}
	/*uint8_t check = 0b00000100;
	while(HAL_I2C_Mem_Write(&hi2c1,ADDRESS,0x24,1,&check,1,100) != HAL_OK){
		debug = 7;


	}*/
#ifdef OFFSET
	uint32_t tmp = HAL_GetTick();
	uint16_t counter = 0;
	while(HAL_GetTick() - tmp < 10){
		getValue();
		for(uint8_t i=0; i<3; i++){
			buf[i] += angularVelocity[i];
		}
		counter += 1;
	}
	for(uint8_t n=0; n<3; n++){
		offset[n] = buf[n] / counter;
	}
	debug = 4;
#endif //OFFSET
}

void loop(void){
	//getValue();
	if(!HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_13)){
		for(uint8_t i=0;i<3;i++){
			deg[i] = 0;
			//debug = 8;
		}
		HAL_GPIO_TogglePin(GPIOA,GPIO_PIN_5);
	}
}

/* Function Body Begin */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){
	if(htim == &htim6){
		getValue();
		setZero();
		warikomi++;
		//HAL_GPIO_TogglePin(GPIOA,GPIO_PIN_5);
#ifdef OFFSET
		for(uint8_t i=0;i<3;i++){
			if(offset[i] - 10 < angularVelocity[i] && angularVelocity[i] < offset[i] + 10){
				//angularVelocity[i] = offset[i];
				//offset[i] = angularVelocity[i];
			}
		}

#endif//OFFSET
		//debug = 6;
		calcAngle();

	}
}
void getValue(){

	if(HAL_I2C_Mem_Read(&hi2c1,ADDRESS,GYRO_REGISTA_START,1,(uint8_t*)data,sizeof(data),0xFFF) == HAL_OK){
		commu++;
	}
	for(uint8_t i=0;i < 3;i++){
		angularVelocity[i] = (int16_t)(data[2*i+1]<<8) | data[2*i];
		angularVelocity[i] -= anguluarVelocityError[i];
		fixAngularvelocity[i] = angularVelocity[i];
		//debug = 5;
		if(data[2*i] == beforeData[2*i]){
			countE++;
		}else{
			countE = 0;
			beforeData[2*i] = data[2*i];
		}
		if(countE > 210){
			init();
			countE = 0;
		}
	}

	//setZero();
}
void calcAngle(){
	for(uint8_t number = 0;number<3;number++){
		float* before;
		float* now;
		float* buf;
		static float accBefore[3] = {0,0,0};
		float factor[3];
		switch(number){
			case 0:
				before = &(intergral.before.roll);
				now = &(intergral.now.roll);
				buf = &(beforeCorrect.roll);
				factor[0] = 1;
				factor[1] = sinf(rad[0])*tanf(rad[1]);
				factor[2] = cosf(rad[0])*tanf(rad[1]);
				break;
			case 1:
				before = &(intergral.before.pitch);
				now = &(intergral.now.pitch);
				buf = &(beforeCorrect.pitch);
				factor[0] = 0;
				factor[1] = cosf(rad[0]);
				factor[2] = -sinf(rad[0]);
				break;
			case 2:
				before = &(intergral.before.yow);
				now = &(intergral.now.yow);
				buf = &(beforeCorrect.yow);
				factor[0] = 0;
				factor[1] = sinf(rad[0])/cosf(rad[1]);
				factor[2] = cosf(rad[0])/cosf(rad[1]);
		}
		*before = *now;
		*buf = (angularVelocity[number] - offset[number])/ 7.0;
		*now = (beforeCorrect.roll*factor[0]+beforeCorrect.pitch*factor[1]+beforeCorrect.yow*factor[2]);
		//*now = *buf;//角度を補正しない
#ifdef LOOPASS
		if((*now - *before)<10 && -10<(*now -*before)){
			loopassGain = 0.5;
		}else{
			loopassGain = 0.01;
		}
		*now -= (1-loopassGain)*(*before) + loopassGain*(*now);
#endif//LOOPASS
		deg[number] += (*before + *now)/FRENQUENCY/2.0;

		if(!HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_13)){
			for(uint8_t i=0;i<3;i++){
				deg[i] = 0;
				//debug = 8;
			}
			HAL_GPIO_TogglePin(GPIOA,GPIO_PIN_5);
		}


		rad[number] = deg[number]/180*M_PI;

	}


}
void setZero(){
	//uint32_t nowtick = HAL_GetTick();
	/*static uint32_t tick = 0;
	static float beforeVelocity[3] = {0,0,0};
	if(oksign[0]+oksign[1]+oksign[2] != 3){
		while(oksign[0]+oksign[1]+oksign[2] != 3){
			debug = 9;
			for(uint8_t i=0;i<3;i++){
				if(!(beforeVelocity[i]-20 < angularVelocity[i] && angularVelocity[i] < beforeVelocity[i]+20)){
					tick = timerCount;
					debugTick = tick;
					beforeVelocity[i] = angularVelocity[i];
				}
				tickRange = timerCount-tick;
				if(timerCount-tick > 200){
					anguluarVelocityError[i]=angularVelocity[i];
					deg[i] = 0;
					oksign[i] = 1;
				}
			}
		}
	}*/
	for(uint8_t i = 0;i<3;i++){

		static uint32_t tick = 0;
		if(errorFlug[i] == 0){

			if(!(beforeVelocity[i]-12 < angularVelocity[i] && angularVelocity[i] < beforeVelocity[i]+12)){
				tick = HAL_GetTick();
				beforeVelocity[i] = angularVelocity[i];
			}
			if(HAL_GetTick()-tick > 1800){
				tickRange = HAL_GetTick()-tick;
				anguluarVelocityError[i] = angularVelocity[i];
				errorFlug[i] = 1;
				deg[i] = 0;
			}
		}
	}

}

/* Function Body End */
