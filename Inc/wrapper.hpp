#ifndef WRAPPER_HPP_
#define WRAPPER_HPP_
#include <stdint.h>
/* Enum Begin */

/* Enum End */


/* Struct Begin */
struct angleVector3D{
	float roll;
	float pitch;
	float yow;
};
struct pastAndNow{
	struct angleVector3D before;
	struct angleVector3D now;
};

/* Struct End */

#ifdef __cplusplus
extern "C" {
#endif

void init(void);
void loop(void);

#ifdef __cplusplus
};
#endif
/* Function Prototype Begin */
void getValue();
void calcAngle();
void setZero();
void MyDelay(uint32_t);
/* Function Prototype End */


#endif /* WRAPPER_HPP_ */
